package com.teleqom.smpp.connection.service;

import com.teleqom.smpp.connection.config.SmppProperties;
import com.teleqom.smpp.connection.exception.SmppBoundStateException;
import com.teleqom.smpp.connection.exception.SmppNullResponseException;
import com.teleqom.smpp.connection.exception.SmppUnboundStateException;
import com.teleqom.smpp.connection.service.impl.ConnectionServiceImpl;
import com.teleqom.smpp.connection.util.MockConnection;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.smpp.Receiver;
import org.smpp.Session;
import org.smpp.TimeoutException;
import org.smpp.WrongSessionStateException;
import org.smpp.pdu.PDUException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("test")
@SpringBootTest
class ConnectionServiceTest {

    private MockConnection connection;

    private Session session;

    private ConnectionService service;

    @Autowired
    private SmppProperties properties;

    @BeforeEach
    void setUp() {
        connection = new MockConnection();
        session = new Session(connection);
        service = new ConnectionServiceImpl(session, properties);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void bindTransceiver_OK() throws SmppBoundStateException, SmppNullResponseException,
            PDUException, TimeoutException, WrongSessionStateException, IOException {
        boolean status = service.bindTransceiver();

        assertTrue(status);
    }

    @Test
    void bindTransceiver_NOK() throws SmppBoundStateException, SmppNullResponseException,
            PDUException, TimeoutException, WrongSessionStateException, IOException {
        connection.setOkResponse(false);

        boolean status = service.bindTransceiver();

        assertFalse(status);
    }

    @Test
    void bindTransceiver_SmppBoundStateException() throws SmppBoundStateException, SmppNullResponseException,
            PDUException, TimeoutException, WrongSessionStateException, IOException {
        boolean status = service.bindTransceiver();

        assertTrue(status);
        assertThrows(SmppBoundStateException.class, () -> service.bindTransceiver());
    }

    @Test
    void bindTransceiver_SmppNullResponseException() {
        connection.setNullResponse(true);
        Receiver receiver = session.getReceiver();
        if (receiver != null) {
            receiver.setReceiveTimeout(5000);
        }

        assertThrows(SmppNullResponseException.class, () -> service.bindTransceiver());
    }

    @Test
    void enquireLink_OK() throws SmppBoundStateException, SmppUnboundStateException, SmppNullResponseException,
            PDUException, TimeoutException, WrongSessionStateException, IOException {
        service.bindTransceiver();

        boolean status = service.enquireLink();

        assertTrue(status);
    }

    @Test
    void enquireLink_NOK() throws SmppBoundStateException, SmppUnboundStateException, SmppNullResponseException,
            PDUException, TimeoutException, WrongSessionStateException, IOException {
        service.bindTransceiver();
        connection.setOkResponse(false);

        boolean status = service.enquireLink();

        assertFalse(status);
    }

    @Test
    void enquireLink_SmppUnboundStateException() {
        assertThrows(SmppUnboundStateException.class, () -> service.enquireLink());
    }

    @Test
    void enquireLink_SmppNullResponseException() throws SmppBoundStateException, SmppNullResponseException,
            PDUException, TimeoutException, WrongSessionStateException, IOException {
        service.bindTransceiver();
        Receiver receiver = session.getReceiver();
        if (receiver != null) {
            receiver.setReceiveTimeout(5000);
        }
        connection.setNullResponse(true);

        assertThrows(SmppNullResponseException.class, () -> service.enquireLink());
    }

    @Test
    void unbindTransceiver_OK() throws SmppBoundStateException, SmppUnboundStateException, SmppNullResponseException,
            PDUException, TimeoutException, WrongSessionStateException, IOException {
        service.bindTransceiver();

        boolean status = service.unbindTransceiver();

        assertTrue(status);
    }

    @Test
    void unbindTransceiver_NOK() throws SmppBoundStateException, SmppUnboundStateException, SmppNullResponseException,
            PDUException, TimeoutException, WrongSessionStateException, IOException {
        service.bindTransceiver();
        connection.setOkResponse(false);

        boolean status = service.unbindTransceiver();

        assertFalse(status);
    }

    @Test
    void unbindTransceiver_SmppUnboundStateException() {
        assertThrows(SmppUnboundStateException.class, () -> service.unbindTransceiver());
    }

    @Test
    void unbindTransceiver_SmppNullResponseException() throws SmppBoundStateException, SmppNullResponseException,
            PDUException, TimeoutException, WrongSessionStateException, IOException {
        service.bindTransceiver();
        Receiver receiver = session.getReceiver();
        if (receiver != null) {
            receiver.setReceiveTimeout(5000);
        }
        connection.setNullResponse(true);

        assertThrows(SmppNullResponseException.class, () -> service.unbindTransceiver());
    }

}