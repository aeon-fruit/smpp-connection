package com.teleqom.smpp.connection.util;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.smpp.Connection;
import org.smpp.pdu.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.smpp.Data.ESME_ROK;

class MockConnectionTest {

    private MockConnection connection;

    @BeforeEach
    void setUp() {
        connection = new MockConnection();
    }

    @AfterEach
    void tearDown() {
        connection.close();
        connection = null;
    }

    @Test
    void open_Once() {
        connection.open();

        assertTrue(connection.isOpened());
    }

    @Test
    void open_Idempotence() {
        int reps = (int) (Math.random() * 18 + 2);
        for (int i = 0; i < reps; ++i) {
            connection.open();
        }

        assertTrue(connection.isOpened());
    }

    @Test
    void close_Once() {
        connection.open();

        connection.close();

        assertFalse(connection.isOpened());
    }

    @Test
    void close_Identity() {
        connection.close();

        assertFalse(connection.isOpened());
    }

    @Test
    void close_Idempotence() {
        connection.open();

        int reps = (int) (Math.random() * 18 + 2);
        for (int i = 0; i < reps; ++i) {
            connection.close();
        }

        assertFalse(connection.isOpened());
    }

    @Test
    void isOpened_Default() {
        assertFalse(connection.isOpened());
    }

    @Test
    void send_BIND_TRANSCEIVER() throws PDUException {
        connection.open();

        connection.send(new BindTransciever().getData());

        assertEquals(BindTranscieverResp.class, PDU.createPDU(connection.receive()).getClass());
    }

    @Test
    void send_ENQUIRE_LINK() throws PDUException {
        bindTransceiver();

        connection.send(new EnquireLink().getData());

        assertEquals(EnquireLinkResp.class, PDU.createPDU(connection.receive()).getClass());
    }

    @Test
    void send_UNBIND() throws PDUException {
        bindTransceiver();

        connection.send(new Unbind().getData());

        assertEquals(UnbindResp.class, PDU.createPDU(connection.receive()).getClass());
    }

    @Test
    void send_BIND_TRANSCEIVER_NotOpen() throws PDUException {
        connection.send(new BindTransciever().getData());

        assertEquals(0, connection.receive().length());
    }

    @Test
    void send_ENQUIRE_LINK_NotOpen() throws PDUException {
        connection.send(new EnquireLink().getData());

        assertEquals(0, connection.receive().length());
    }

    @Test
    void send_UNBIND_NotOpen() throws PDUException {
        connection.send(new Unbind().getData());

        assertEquals(0, connection.receive().length());
    }

    @Test
    void send_UnknownPDU_NotOpen() throws PDUException {
        connection.send(new BindTransmitter().getData());

        assertEquals(0, connection.receive().length());
    }

    @Test
    void send_NullBuffer_NotOpen() {
        connection.send(null);

        assertEquals(0, connection.receive().length());
    }

    @Test
    void send_BIND_TRANSCEIVER_AlreadyBound() throws PDUException {
        bindTransceiver();

        connection.send(new BindTransciever().getData());

        assertEquals(0, connection.receive().length());
    }

    @Test
    void send_UnknownPDU_AlreadyBound() throws PDUException {
        bindTransceiver();

        connection.send(new BindTransmitter().getData());

        assertEquals(0, connection.receive().length());
    }

    @Test
    void send_NullBuffer_AlreadyBound() throws ValueNotSetException {
        bindTransceiver();

        connection.send(null);

        assertEquals(0, connection.receive().length());
    }

    @Test
    void send_ENQUIRE_LINK_Unbound() throws PDUException {
        connection.open();

        connection.send(new EnquireLink().getData());

        assertEquals(0, connection.receive().length());
    }

    @Test
    void send_UNBIND_Unbound() throws PDUException {
        connection.open();

        connection.send(new Unbind().getData());

        assertEquals(0, connection.receive().length());
    }

    @Test
    void send_UnknownPDU_Unbound() throws PDUException {
        connection.open();

        connection.send(new BindTransmitter().getData());

        assertEquals(0, connection.receive().length());
    }

    @Test
    void send_NullBuffer_Unbound() {
        connection.open();

        connection.send(null);

        assertEquals(0, connection.receive().length());
    }

    @Test
    void receive_Default() {
        assertEquals(0, connection.receive().length());
    }

    @Test
    void receive_Default_Open() {
        assertEquals(0, connection.receive().length());
    }

    @Test
    void receive_Default_Extra() throws ValueNotSetException {
        connection.open();
        bindTransceiver();

        assertEquals(0, connection.receive().length());
    }

    @Test
    void accept() {
        Connection obj = connection.accept();

        assertNotNull(obj);
        assertEquals(MockConnection.class, obj.getClass());
    }

    @Test
    void setOkResponse_Default() throws PDUException {
        connection.open();

        connection.send(new BindTransciever().getData());

        assertEquals(ESME_ROK, PDU.createPDU(connection.receive()).getCommandStatus());
    }

    @Test
    void setOkResponse_True() throws PDUException {
        connection.open();

        connection.setOkResponse(true);
        connection.send(new BindTransciever().getData());

        assertEquals(ESME_ROK, PDU.createPDU(connection.receive()).getCommandStatus());
    }

    @Test
    void setOkResponse_False() throws PDUException {
        connection.open();

        connection.setOkResponse(false);
        connection.send(new BindTransciever().getData());

        assertNotEquals(ESME_ROK, PDU.createPDU(connection.receive()).getCommandStatus());
    }

    @Test
    void setNullResponse_Default() throws PDUException {
        connection.open();

        connection.send(new BindTransciever().getData());

        assertEquals(BindTranscieverResp.class, PDU.createPDU(connection.receive()).getClass());
    }

    @Test
    void setNullResponse_True() throws PDUException {
        connection.open();

        connection.setNullResponse(true);
        connection.send(new BindTransciever().getData());

        assertEquals(0, connection.receive().length());
    }

    @Test
    void setNullResponse_False() throws PDUException {
        connection.open();

        connection.setNullResponse(false);
        connection.send(new BindTransciever().getData());

        assertEquals(BindTranscieverResp.class, PDU.createPDU(connection.receive()).getClass());
    }

    private void bindTransceiver() throws ValueNotSetException {
        connection.open();
        connection.send(new BindTransciever().getData());
        connection.receive();
    }

}