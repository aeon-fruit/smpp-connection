package com.teleqom.smpp.connection.controller;

import com.teleqom.smpp.connection.exception.SmppBoundStateException;
import com.teleqom.smpp.connection.exception.SmppNullResponseException;
import com.teleqom.smpp.connection.exception.SmppUnboundStateException;
import com.teleqom.smpp.connection.service.ConnectionService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.http.HttpStatus.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest
class ConnectionControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    ConnectionService service;

    @Test
    void bindTransceiver_OK() throws Exception {
        Mockito.when(service.bindTransceiver()).thenReturn(true);

        mockMvc.perform(post("/api/v1/connection/"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().json("{\"status\": true}"));
    }

    @Test
    void bindTransceiver_NOK() throws Exception {
        Mockito.when(service.bindTransceiver()).thenReturn(false);

        mockMvc.perform(post("/api/v1/connection/"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().json("{\"status\": false}"));
    }

    @Test
    void bindTransceiver_SmppBoundStateException() throws Exception {
        Mockito.when(service.bindTransceiver()).thenThrow(new SmppBoundStateException());

        mockMvc.perform(post("/api/v1/connection/"))
                .andExpect(status().isConflict())
                .andExpect(jsonPath("$.status", org.hamcrest.Matchers.is(CONFLICT.name())))
                .andExpect(jsonPath("$.debug_message", org.hamcrest.Matchers.containsString("already bound")));
    }

    @Test
    void bindTransceiver_SmppNullResponseException() throws Exception {
        Mockito.when(service.bindTransceiver()).thenThrow(new SmppNullResponseException("Expected exception"));

        mockMvc.perform(post("/api/v1/connection/"))
                .andExpect(status().isInternalServerError())
                .andExpect(jsonPath("$.status", org.hamcrest.Matchers.is(INTERNAL_SERVER_ERROR.name())))
                .andExpect(jsonPath("$.debug_message", org.hamcrest.Matchers.containsString("ted except")));
    }

    @Test
    void enquireLink_OK() throws Exception {
        Mockito.when(service.enquireLink()).thenReturn(true);

        mockMvc.perform(get("/api/v1/connection/"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().json("{\"status\": true}"));
    }

    @Test
    void enquireLink_NOK() throws Exception {
        Mockito.when(service.enquireLink()).thenReturn(false);

        mockMvc.perform(get("/api/v1/connection/"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().json("{\"status\": false}"));
    }

    @Test
    void enquireLink_SmppUnboundStateException() throws Exception {
        Mockito.when(service.enquireLink()).thenThrow(new SmppUnboundStateException());

        mockMvc.perform(get("/api/v1/connection/"))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.status", org.hamcrest.Matchers.is(NOT_FOUND.name())))
                .andExpect(jsonPath("$.debug_message", org.hamcrest.Matchers.containsString("not bound")));
    }

    @Test
    void enquireLink_SmppNullResponseException() throws Exception {
        Mockito.when(service.enquireLink()).thenThrow(new SmppNullResponseException("Another exception"));

        mockMvc.perform(get("/api/v1/connection/"))
                .andExpect(status().isInternalServerError())
                .andExpect(jsonPath("$.status", org.hamcrest.Matchers.is(INTERNAL_SERVER_ERROR.name())))
                .andExpect(jsonPath("$.debug_message", org.hamcrest.Matchers.containsString("her except")));
    }

    @Test
    void unbindTransceiver_OK() throws Exception {
        Mockito.when(service.unbindTransceiver()).thenReturn(true);

        mockMvc.perform(delete("/api/v1/connection/"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().json("{\"status\": true}"));
    }

    @Test
    void unbindTransceiver_NOK() throws Exception {
        Mockito.when(service.unbindTransceiver()).thenReturn(false);

        mockMvc.perform(delete("/api/v1/connection/"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().json("{\"status\": false}"));
    }

    @Test
    void unbindTransceiver_SmppUnboundStateException() throws Exception {
        Mockito.when(service.unbindTransceiver()).thenThrow(new SmppUnboundStateException());

        mockMvc.perform(delete("/api/v1/connection/"))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.status", org.hamcrest.Matchers.is(NOT_FOUND.name())))
                .andExpect(jsonPath("$.debug_message", org.hamcrest.Matchers.containsString("not bound")));
    }

    @Test
    void unbindTransceiver_SmppNullResponseException() throws Exception {
        Mockito.when(service.unbindTransceiver()).thenThrow(new SmppNullResponseException("One more exception"));

        mockMvc.perform(delete("/api/v1/connection/"))
                .andExpect(status().isInternalServerError())
                .andExpect(jsonPath("$.status", org.hamcrest.Matchers.is(INTERNAL_SERVER_ERROR.name())))
                .andExpect(jsonPath("$.debug_message", org.hamcrest.Matchers.containsString("ore except")));
    }

}
