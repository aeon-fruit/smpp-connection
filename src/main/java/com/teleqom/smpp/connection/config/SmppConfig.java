package com.teleqom.smpp.connection.config;

import com.teleqom.smpp.connection.util.MockConnection;
import org.smpp.Connection;
import org.smpp.SSLConnection;
import org.smpp.Session;
import org.smpp.TCPIPConnection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SmppConfig {

    private final SmppProperties props;

    @Autowired
    public SmppConfig(SmppProperties props) {
        this.props = props;
    }

    @Bean
    public Connection connection() {
        boolean stub = Boolean.parseBoolean(props.getStub());
        if (stub) {
            return new MockConnection();
        }
        String address = props.getAddress();
        int port = Integer.parseInt(props.getPort());
        boolean useSSL = Boolean.parseBoolean(props.getUseSSL());
        return useSSL ? new SSLConnection(address, port) : new TCPIPConnection(address, port);
    }

    @Bean
    public Session session() {
        return new Session(connection());
    }

}


