package com.teleqom.smpp.connection.config;

import com.teleqom.smpp.connection.exception.SmppBoundStateException;
import com.teleqom.smpp.connection.exception.SmppNullResponseException;
import com.teleqom.smpp.connection.exception.SmppUnboundStateException;
import com.teleqom.smpp.connection.util.ApiError;
import org.smpp.TimeoutException;
import org.smpp.WrongSessionStateException;
import org.smpp.pdu.PDUException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.io.IOException;

@ControllerAdvice
public class CustomResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(SmppNullResponseException.class)
    protected ResponseEntity<Object> handleIOException(SmppNullResponseException exception, WebRequest request) {
        return buildResponseEntity(new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, exception), exception, request);
    }

    @ExceptionHandler(SmppBoundStateException.class)
    protected ResponseEntity<Object> handleResourceBadRequestException(SmppBoundStateException exception, WebRequest request) {
        return buildResponseEntity(new ApiError(HttpStatus.CONFLICT, exception), exception, request);
    }

    @ExceptionHandler(SmppUnboundStateException.class)
    protected ResponseEntity<Object> handleResourceNotFoundException(SmppUnboundStateException exception, WebRequest request) {
        return buildResponseEntity(new ApiError(HttpStatus.NOT_FOUND, exception), exception, request);
    }

    @ExceptionHandler(PDUException.class)
    protected ResponseEntity<Object> handleIOException(PDUException exception, WebRequest request) {
        return buildResponseEntity(new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, exception), exception, request);
    }

    @ExceptionHandler(TimeoutException.class)
    protected ResponseEntity<Object> handleIOException(TimeoutException exception, WebRequest request) {
        return buildResponseEntity(new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, exception), exception, request);
    }

    @ExceptionHandler(WrongSessionStateException.class)
    protected ResponseEntity<Object> handleIOException(WrongSessionStateException exception, WebRequest request) {
        return buildResponseEntity(new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, exception), exception, request);
    }

    @ExceptionHandler(IOException.class)
    protected ResponseEntity<Object> handleIOException(IOException exception, WebRequest request) {
        return buildResponseEntity(new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, exception), exception, request);
    }

    private ResponseEntity<Object> buildResponseEntity(ApiError apiError, Exception exception, WebRequest request) {
        return handleExceptionInternal(exception, apiError, new HttpHeaders(), apiError.getStatus(), request);
    }

}

