package com.teleqom.smpp.connection.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@ConfigurationProperties(prefix = "smpp")
@Configuration("smpp")
@Getter
@Setter
public class SmppProperties {

    private String address;
    private String port;
    private String useSSL;
    private String systemId;
    private String password;
    private String stub;

}
