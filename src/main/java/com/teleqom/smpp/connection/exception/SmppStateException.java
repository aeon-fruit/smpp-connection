package com.teleqom.smpp.connection.exception;

public abstract class SmppStateException extends Exception {

    public SmppStateException(String message) {
        super(message);
    }

}
