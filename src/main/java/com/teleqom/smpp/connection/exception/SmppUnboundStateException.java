package com.teleqom.smpp.connection.exception;

public class SmppUnboundStateException extends SmppStateException {

    public SmppUnboundStateException() {
        super("Session is not bound");
    }

}
