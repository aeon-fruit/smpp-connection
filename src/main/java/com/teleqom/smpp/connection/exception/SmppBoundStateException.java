package com.teleqom.smpp.connection.exception;

public class SmppBoundStateException extends SmppStateException {

    public SmppBoundStateException() {
        super("Session is already bound");
    }

}
