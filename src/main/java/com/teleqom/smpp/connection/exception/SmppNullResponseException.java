package com.teleqom.smpp.connection.exception;

public class SmppNullResponseException extends Exception {

    public SmppNullResponseException(String message) {
        super(message);
    }

}
