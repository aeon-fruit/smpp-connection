package com.teleqom.smpp.connection.util;

import org.smpp.Connection;
import org.smpp.Data;
import org.smpp.pdu.*;
import org.smpp.util.ByteBuffer;

import java.util.Optional;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.smpp.Data.ESME_RUNKNOWNERR;

public class MockConnection extends Connection {

    private final AtomicBoolean opened = new AtomicBoolean(false);
    private final AtomicBoolean bound = new AtomicBoolean(false);

    private final LinkedBlockingQueue<Optional<Response>> responses = new LinkedBlockingQueue<>();
    private final AtomicBoolean okResponse = new AtomicBoolean(true);
    private final AtomicBoolean nullResponse = new AtomicBoolean(false);

    public MockConnection() {
        super();
    }

    @Override
    public void open() {
        opened.compareAndSet(false, true);
    }

    @Override
    public void close() {
        bound.compareAndSet(true, false);
        opened.compareAndSet(true, false);
    }

    @Override
    public boolean isOpened() {
        return opened.get();
    }

    @Override
    public void send(ByteBuffer data) {
        if (data == null || !opened.get()) {
            send(null, false);
            return;
        }
        try {
            PDU pdu = PDU.createPDU(data);
            if (pdu.isRequest()) {
                Request req = (Request) pdu;
                switch (req.getCommandId()) {
                    case Data.BIND_TRANSCEIVER: {
                        send(req, !bound.getAndSet(true));
                        return;
                    }
                    case Data.ENQUIRE_LINK: {
                        send(req, bound.get());
                        return;
                    }
                    case Data.UNBIND: {
                        send(req, bound.getAndSet(false));
                        opened.compareAndSet(true, false);
                        return;
                    }
                }
            }
        } catch (PDUException e) {
            e.printStackTrace();
        }
        send(null, false);
    }

    @Override
    public ByteBuffer receive() {
        try {
            if (responses.isEmpty()) {
                Thread.sleep(1000);
                return new ByteBuffer();
            }
            Optional<Response> optional = responses.take();
            Response resp = optional.orElse(null);
            if (resp != null) {
                return resp.getData();
            }
        } catch (ValueNotSetException e) {
            e.printStackTrace();
        } catch (InterruptedException ignored) {
        }
        return new ByteBuffer();
    }

    @Override
    public Connection accept() {
        return new MockConnection();
    }

    private synchronized void send(Request req, boolean bindingState) {
        Response resp = null;
        if ((req != null) && bindingState) {
            resp = req.getResponse();
            if (!okResponse.get()) {
                resp.setCommandStatus(ESME_RUNKNOWNERR);
            }
            if (nullResponse.get()) {
                resp = null;
            }
        }
        try {
            responses.put(Optional.ofNullable(resp));
        } catch (InterruptedException ignored) {
        }
    }

    public void setOkResponse(boolean okResponse) {
        this.okResponse.getAndSet(okResponse);
    }

    public void setNullResponse(boolean nullResponse) {
        this.nullResponse.getAndSet(nullResponse);
    }

}
