package com.teleqom.smpp.connection;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmppConnectionApplication {

    public static void main(String[] args) {
        SpringApplication.run(SmppConnectionApplication.class, args);
    }

}
