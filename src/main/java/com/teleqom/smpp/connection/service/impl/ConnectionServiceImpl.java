package com.teleqom.smpp.connection.service.impl;

import com.teleqom.smpp.connection.config.SmppProperties;
import com.teleqom.smpp.connection.exception.SmppBoundStateException;
import com.teleqom.smpp.connection.exception.SmppNullResponseException;
import com.teleqom.smpp.connection.exception.SmppUnboundStateException;
import com.teleqom.smpp.connection.service.ConnectionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smpp.Session;
import org.smpp.TimeoutException;
import org.smpp.WrongSessionStateException;
import org.smpp.pdu.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class ConnectionServiceImpl implements ConnectionService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConnectionServiceImpl.class);

    private final Session session;
    private final SmppProperties smppProps;

    @Autowired
    public ConnectionServiceImpl(Session session, SmppProperties smppProps) {
        this.session = session;
        this.smppProps = smppProps;
    }

    @Override
    public boolean bindTransceiver() throws SmppBoundStateException, SmppNullResponseException,
            PDUException, TimeoutException, WrongSessionStateException, IOException {
        LOGGER.info("bindTransceiver - IN");

        if (session.isBound()) {
            LOGGER.error("bindTransceiver - OUT: session already bound");
            throw new SmppBoundStateException();
        }

        BindRequest req = new BindTransciever();
        req.setSystemId(smppProps.getSystemId());
        req.setPassword(smppProps.getPassword());
        req.setSystemType("Spring");
        LOGGER.debug("bindTransceiver: request={}", req.debugString());

        BindResponse resp = session.bind(req);
        if (resp == null) {
            LOGGER.error("bindTransceiver - OUT: null response");
            throw new SmppNullResponseException("Session.bind request: " + req.debugString());
        }
        LOGGER.debug("bindTransceiver: response={}, status={}", resp.debugString(), resp.getCommandStatus());

        boolean bound = resp.isOk();
        LOGGER.info("bindTransceiver - OUT: bound={}", bound);
        return bound;
    }

    @Override
    public boolean enquireLink() throws SmppUnboundStateException, SmppNullResponseException,
            PDUException, TimeoutException, WrongSessionStateException, IOException {
        LOGGER.info("enquireLink - IN");

        if (!session.isBound()) {
            LOGGER.error("enquireLink - OUT: session not bound");
            throw new SmppUnboundStateException();
        }

        EnquireLinkResp resp = session.enquireLink();
        if (resp == null) {
            LOGGER.error("enquireLink - OUT: null response");
            throw new SmppNullResponseException("Session.enquireLink");
        }
        LOGGER.debug("enquireLink: response={}, status={}", resp.debugString(), resp.getCommandStatus());

        boolean alive = resp.isOk();
        LOGGER.info("enquireLink - OUT: alive={}", alive);
        return alive;
    }

    @Override
    public boolean unbindTransceiver() throws SmppUnboundStateException, SmppNullResponseException,
            PDUException, TimeoutException, WrongSessionStateException, IOException {
        LOGGER.info("unbindTransceiver - IN");

        if (!session.isBound()) {
            LOGGER.error("unbindTransceiver - OUT: session not bound");
            throw new SmppUnboundStateException();
        }

        UnbindResp resp = session.unbind();
        if (resp == null) {
            LOGGER.error("unbindTransceiver - OUT: null response");
            throw new SmppNullResponseException("Session.unbind");
        }
        LOGGER.debug("unbindTransceiver: response={}, status={}", resp.debugString(), resp.getCommandStatus());

        boolean unbound = resp.isOk();
        LOGGER.info("unbindTransceiver - OUT: unbound={}", unbound);
        return unbound;
    }

}
