package com.teleqom.smpp.connection.service;

import com.teleqom.smpp.connection.exception.SmppBoundStateException;
import com.teleqom.smpp.connection.exception.SmppNullResponseException;
import com.teleqom.smpp.connection.exception.SmppUnboundStateException;
import org.smpp.TimeoutException;
import org.smpp.WrongSessionStateException;
import org.smpp.pdu.PDUException;

import java.io.IOException;

public interface ConnectionService {

    boolean bindTransceiver() throws SmppBoundStateException, SmppNullResponseException,
            PDUException, TimeoutException, WrongSessionStateException, IOException;

    boolean enquireLink() throws SmppUnboundStateException, SmppNullResponseException,
            PDUException, TimeoutException, WrongSessionStateException, IOException;

    boolean unbindTransceiver() throws SmppUnboundStateException, SmppNullResponseException,
            PDUException, TimeoutException, WrongSessionStateException, IOException;

}
