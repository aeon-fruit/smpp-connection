package com.teleqom.smpp.connection.controller;

import com.teleqom.smpp.connection.dto.StatusDto;
import com.teleqom.smpp.connection.service.ConnectionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/connection")
public class ConnectionController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConnectionController.class);

    private final ConnectionService service;

    @Autowired
    public ConnectionController(ConnectionService service) {
        this.service = service;
    }

    @PostMapping("/")
    public ResponseEntity<StatusDto> bindTransceiver() throws Exception {
        boolean status = service.bindTransceiver();
        return ResponseEntity.ok(new StatusDto(status));
    }

    @GetMapping("/")
    public ResponseEntity<StatusDto> enquireLink() throws Exception {
        boolean status = service.enquireLink();
        return ResponseEntity.ok(new StatusDto(status));
    }

    @DeleteMapping("/")
    public ResponseEntity<StatusDto> unbindTransceiver() throws Exception {
        boolean status = service.unbindTransceiver();
        return ResponseEntity.ok(new StatusDto(status));
    }

}
