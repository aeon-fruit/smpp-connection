FROM openjdk:11-jre-slim
LABEL maintainer="Atef N. <aeon.fruit@programmer.net>"

WORKDIR /app
ADD target/smpp-connection-0.1.0.jar .

ENTRYPOINT ["java"]
CMD ["-jar", "/app/smpp-connection-0.1.0.jar"]
EXPOSE 8080