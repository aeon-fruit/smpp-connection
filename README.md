# SMPP Connection Service

This application is a demo for the integration of Spring Web and the OpenSMPP client, a library for developing Java SMPP clients.

## Overview in layman words

SMPP is a protocol that specifies how your computer should speak to a provider of SMS messaging service. 
This demo application exposes three commands from the protocol.

What do those commands tell the provider?

Consider yourself calling someone on the phone (you=the service at hand).  
Once the other person (interlocutor=the provider) picks up the phone, 
you need to initiate the conversation by saying 'Hello' (hello=bind_transceiver).

Let us assume that you are the silent type and that the person you are speaking to has a long story to tell.
In that case, you will need to announce your presence on the line by saying 'Aha' and 'Yes' every few seconds (aha/yes=enquire_link). 
Otherwise, the other person wouldn't tell whether you are still there or not, and could decide to hang up the phone.

Finally, before hanging up the phone yourself, you need to gracefully end the call by saying 'Goodbye' (goodbye=unbind).

This service has three HTTP endpoints (channels for requesting actions from the service). Each endpoint handles exactly one of those commands.

## Overview

The service samples three commands of the SMPP protocol to communicate with a provider (SMSC or SMS Gateway):

| Command          | URI             | Method   | Request | Response                               |
|------------------|-----------------|----------|---------|----------------------------------------|
| bind_transceiver | /api/v1/connect | POST     | None    | `{status: boolean}` true if successful |
| enquire_link     | /api/v1/connect | GET      | None    | `{status: boolean}` true if successful |
| unbind           | /api/v1/connect | DELETE   | None    | `{status: boolean}` true if successful |

Details on the protocol could be found [here](https://smpp.org/).

## Running the application

The application expects some environment variables. Those are defined in sc-env:

| Variable       | Description                 | Default value |
|----------------|-----------------------------|---------------|
| SMPP_HOST      | Provider host name          | 127.0.0.1     |
| SMPP_PORT      | Provider port               | 2776          |
| SMPP_USE_SSL   | Use SSL/TLS                 | false         |
| SMPP_SYSTEM_ID | Client systemId for auth    | smppclient1   |
| SMPP_PASSWORD  | Client password for auth    | password      |
| SMPP_STUB      | Stub connection to the SMSC | true          |

If `SMPP_STUB` is `true`, the remaining environment variables are ignored, and the connection to the SMSC is mocked using [MockConnection](https://gitlab.com/aeon-fruit/smpp-connection/-/blob/master/src/main/java/com/teleqom/smpp/connection/util/MockConnection.java) class.

There is a Dockerfile for building an image that contains the application. 
The sc-env file could be passed to `docker run` command to override the default values of the environment variables.

To run the server in a docker container, first build the jar file, then the docker image:
```shell script
docker build -t smpp-client .
```

Then run:
```shell script
docker run --rm -it -p 8080:8080 --env-file sc-env --name smpp-client smpp-client
```

## Running the simulator

The application was tested on the localhost against [SMPPSIM-Selenium-Tool](https://github.com/bilalahmed54/SMPPSIM-Selenium-Tool) SMSC simulator by switching `SMPP_STUB` to `false` and keeping the default values for the other variables.

The Dockerfile `Dockerfile-sim` is meant for building a docker image for running the server simulator.

Run this command to build the image:
```shell script
docker build -f Dockerfile-sim -t smppsim-selenium .
```

Then:
```shell script
docker run --rm -it -p 2776:2776 --name smppsim-selenium smppsim-selenium
```

A fully dockerized setting could be achieved easily using docker-compose or by simply creating a network to be shared by the application and the simulator containers.
